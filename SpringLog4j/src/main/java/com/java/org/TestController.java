package com.java.org;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TestController {
 
    private Logger logger = Logger.getLogger(TestController.class);
 
    @RequestMapping(value = {"/", "hello"}, method = RequestMethod.GET)
    public ModelAndView helloWorld() {
 
        ModelAndView model = new ModelAndView("helloWorld");
        logger.info("This Is An Info Log Entry ......!");
        logger.error("This Is An Error Log Entry ......!");
        return model;
    }
}
